# Design Consulting

> TODO: Describe the product we want to develop, service we want to offer or system (e.g. business) we want to establish.
>
> Try to use no more than few sentences - elevator pitch style.

## What value do we want to bring and to whom?

> TODO: How does it make the world better? 
> 
> List things that we want to improve or problems that we want to solve. Remember to specify the beneficients (nothing is absolutely good but each thing can be good for something or to someone).

## What are the possible pitfalls?

> TODO: What can go wrong?
>
> Use your experience and imagination to list possible reasons we may fail or it can backfire. Can it be harmful to anyone? What are the tricky parts?

## Who can help us with it?

> TODO: List groups and individuals who can help us in anyway.
> 
> In the beginning feedback is crucial. Who can we ask for it? Who might be interested in participating?

## Related concepts and activities

> TODO: List related concepts (our own or otherwise) or activities.

